/**
 * Created by Simon on 2016-02-29.
 */
import org.bitbucket.talbots.mather.*;

public class Main {
    public static void main(String[] args){
        MathematicalObject monObjet = new Multiplication(
                new Const(2),
                new Power(
                        new Var("x"),
                        new Const(2)
                )
        );
        ObjectValue[] lesObjets = new ObjectValue[1];
        lesObjets[0] = new ObjectValue(new Var("x"), 4);
        double resultat = monObjet.GetResult(lesObjets);
        System.out.print(resultat);
        monObjet = new Sqrt(
                new Substract(
                        new Const(1),
                        new Power(
                                new Var("x"),
                                new Const(2)
                        )
                )
        );
        double sum = monObjet.RiemannSum(new Var("x"),1000000, 0, 1);
        sum *= 4;
        System.out.print("\n" + sum);
    }
}
