package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public abstract class Function extends MathematicalObject {
    protected MathematicalObject[] _innerParts;
    public boolean Replace(MathematicalObject partToReplace, MathematicalObject part){
        for (int i = 0; i < _innerParts.length; i++){
            if(_innerParts[i].equals(partToReplace)){
                _innerParts[i] = part;
            }
            else{
                _innerParts[i].Replace(partToReplace, part);
            }
        }
        return true;
    }
    protected double[] GetPartsResult(ObjectValue[] lesObjets){
        double[] lesResultats = new double[_innerParts.length];
        boolean objetTrouve;
        int x;
        for(int i = 0; i < _innerParts.length; i++){
            objetTrouve = false;
            x = 0;
            while(!objetTrouve && x < lesObjets.length){
                if(_innerParts[i].equals(lesObjets[x].GetObject())){
                    objetTrouve = true;
                    lesResultats[i] = lesObjets[x].GetValue();
                }
                x++;
            }
            if(!objetTrouve){
                lesResultats[i] = _innerParts[i].GetResult(lesObjets);
            }
        }
        return lesResultats;
    }

    @Override
    public boolean equals(Object obj) {
        boolean areEqual = true;
        int i = 0;
        if((obj instanceof Function) && ((Function) obj)._innerParts.length == _innerParts.length){
            while(areEqual && i < _innerParts.length){
                areEqual = _innerParts[i].equals(((Function) obj)._innerParts[i]);
            }
            return areEqual;
        }
        return false;
    }
    @Override
    public boolean IsConstant() {
        boolean isConstant = true;
        int i = 0;
        while(isConstant && i < _innerParts.length){
            isConstant = _innerParts[i].IsConstant();
            i++;
        }
        return isConstant;
    }
}
