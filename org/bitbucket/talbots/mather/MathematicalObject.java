package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public abstract class MathematicalObject {
    public abstract double GetResult(ObjectValue[] lesObjets);
    public abstract MathematicalObject Derive();
    public abstract boolean Replace(MathematicalObject partToReplace, MathematicalObject part);
    public abstract boolean IsConstant();
    public double RiemannSum(MathematicalObject obj, int nbRectangles, double minValue, double maxValue){
        double sum = 0;
        double width = (maxValue - minValue)/nbRectangles;
        double value;
        ObjectValue[] tableauObjets = new ObjectValue[1];
        for(int i = 1; i <= nbRectangles; i++){
            value = minValue + i*width;
            tableauObjets[0] = new ObjectValue(obj, value);
            sum += this.GetResult(tableauObjets) * width;
        }
        return sum;
    }
}
