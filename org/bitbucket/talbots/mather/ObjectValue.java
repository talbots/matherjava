package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-29.
 */
public class ObjectValue {
    private MathematicalObject _obj;
    public double _value;
    public ObjectValue(MathematicalObject obj, double value){
        _obj = obj;
        _value = value;
    }
    public MathematicalObject GetObject(){
        return _obj;
    }
    public double GetValue(){
        return _value;
    }
}
