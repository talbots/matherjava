package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Substract extends SignFunction {
    public Substract(MathematicalObject obj1, MathematicalObject obj2){
        super(obj1, obj2);
    }
    @Override
    public MathematicalObject Derive() {
        return new Substract(_innerParts[0].Derive(), _innerParts[1].Derive());
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return lesResultats[0] - lesResultats[1];
    }
}
