package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Sqrt;

/**
 * Created by Simon on 2016-03-07.
 */
public class FonctionSqrt implements IFonction {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1) {
        return new Sqrt(obj1);
    }
}
