package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Substract;

/**
 * Created by Simon on 2016-03-07.
 */
public class SignSubstract implements ISign{
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1, MathematicalObject obj2) {
        return new Substract(obj1, obj2);
    }
}
