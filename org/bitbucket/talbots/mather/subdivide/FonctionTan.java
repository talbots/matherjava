package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Tan;

/**
 * Created by Simon on 2016-03-07.
 */
public class FonctionTan implements IFonction {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1) {
        return new Tan(obj1);
    }
}
