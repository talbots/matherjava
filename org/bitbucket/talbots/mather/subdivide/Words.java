package org.bitbucket.talbots.mather.subdivide;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Multiplication;

/**
 * Created by Simon on 2016-03-02.
 */
public class Words extends SubdivisionClass {
    private FonctionInterface[] _lesFonctions;
    public Words(String str){
        super(str);
        _lesFonctions = new FonctionInterface[6];
        _lesFonctions[0] = new FonctionInterface(new FonctionSin(), "sin");
        _lesFonctions[1] = new FonctionInterface(new FonctionCos(), "cos");
        _lesFonctions[2] = new FonctionInterface(new FonctionTan(), "tan");
        _lesFonctions[3] = new FonctionInterface(new FonctionLn(), "ln");
        _lesFonctions[4] = new FonctionInterface(new FonctionLog(), "log");
        _lesFonctions[5] = new FonctionInterface(new FonctionSqrt(), "sqrt");
    }
    @Override
    public MathematicalObject GetMathematicalObjet() {
        return GetMathematicalObjet(_str, 0);
    }
    public MathematicalObject GetMathematicalObjet(String str, Integer indiceFonction) throws SyntaxException{
        if(indiceFonction >= _lesFonctions.length){
            PowerSign lExposant = new PowerSign(str);
            return lExposant.GetMathematicalObjet();
        }
        int parenthesis = 0;
        String part = "";
        MathematicalObject objetMathematique = null;
        boolean terminee = false;
        boolean estTerminee;
        boolean valid;
        int i;
        for(i = 0; i< str.length() && !terminee; i++){
            if(str.charAt(i) == '('){
                parenthesis++;
                part += "(";
            }
            else if(str.charAt(i) == ')'){
                parenthesis--;
                part += ")";
            }
            else if(parenthesis == 0){
                valid = true;
                for(int j = 0; j < _lesFonctions[indiceFonction].RetournerMot().length() && valid; j++){
                    if(_lesFonctions[indiceFonction].RetournerMot().charAt(j) != str.charAt(i+j)){
                        valid = false;
                    }
                }
                if(valid){
                    terminee = true;
                }
                else{
                    part += str.charAt(i);
                }
            }
            else{
                part+= str.charAt(i);
            }
        }
        if(terminee){
            if(part != ""){
                objetMathematique = GetMathematicalObjet(part,indiceFonction +1);
                part = "";
                i--;
                i+= _lesFonctions[indiceFonction].RetournerMot().length();
            }
            else{
                i = _lesFonctions[indiceFonction].RetournerMot().length();
            }
            if(str.charAt(i) == '('){
                i++;
                parenthesis = 1;
                estTerminee = false;
                while(i < str.length() && !estTerminee){
                    if(str.charAt(i) == '('){
                        parenthesis++;
                        part += "(";
                    }
                    else if(str.charAt(i) == ')'){
                        parenthesis--;
                        if(parenthesis == 0){
                            estTerminee = true;
                        }
                        else{
                            part += ")";
                        }
                    }
                    else{
                        part+=str.charAt(i);
                    }
                    i++;
                }
            }
            else{
                part += str.charAt(i);
                i++;
            }
            if(part != ""){
                Signs lesSignes = new Signs(part);
                MathematicalObject nouvellePartie = _lesFonctions[indiceFonction].RetournerObjetMathematique(
                        lesSignes.GetMathematicalObjet()
                );
                if( objetMathematique != null){
                    objetMathematique = new Multiplication(objetMathematique, nouvellePartie);
                }
                else{
                    objetMathematique = nouvellePartie;
                }
                part = "";
                for(int j =i; j < str.length(); j++){
                    part += str.charAt(j);
                }
                if(part != ""){
                    objetMathematique = new Multiplication(objetMathematique, GetMathematicalObjet(part, indiceFonction));
                }
            }
            else{
                throw new SyntaxException("Rien entre les parenthèse");
            }
        }
        else{
            return GetMathematicalObjet(str, indiceFonction + 1);
        }
        return objetMathematique;
    }

}
