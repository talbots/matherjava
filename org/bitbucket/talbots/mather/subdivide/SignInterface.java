package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public class SignInterface {
    private char _signe;
    private ISign _uneInterface;
    public SignInterface(ISign uneInterface, char signe){
        _signe = signe;
    }
    public char RetournerChar(){
        return _signe;
    }
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1, MathematicalObject obj2){
        return _uneInterface.RetournerObjetMathematique(obj1, obj2);
    }
}
