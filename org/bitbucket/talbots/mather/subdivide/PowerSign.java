package org.bitbucket.talbots.mather.subdivide;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import org.bitbucket.talbots.mather.Const;
import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Power;

/**
 * Created by Simon on 2016-03-10.
 */
public class PowerSign extends SubdivisionClass {
    public PowerSign(String str){
        super(str);
    }
    @Override
    public MathematicalObject GetMathematicalObjet() {
        return GetMathematicalObjet(_str);
    }
    public MathematicalObject GetMathematicalObjet(String str){
        boolean terminee = false;
        String part = "";
        int parenthesis = 0;
        MathematicalObject objetMathematique = null;
        int i;
        for(i = 0; i < str.length() && !terminee; i++){
            if(str.charAt(i) == '('){
                parenthesis++;
                part += "(";
            }
            else if(str.charAt(i) == ')'){
                parenthesis--;
                part += ")";
            }
            else if(str.charAt(i) == '^' && parenthesis == 0){
                terminee = true;
            }
            else{
                part+= str.charAt(i);
            }
        }
        if(terminee){
            if(part != ""){
                //Do something higher level
                objetMathematique = new Const(3);
                part = "";
                for(int j = i; j < str.length(); j++){
                    part+= str.charAt(j);
                }
                if(part != ""){
                    objetMathematique = new Power(objetMathematique, GetMathematicalObjet(part));
                }
                else{
                    throw new SyntaxException("Rien après l'exposant");
                }
            }
            else{
                throw new SyntaxException("Rien avant l'exposant");
            }
        }
        else{
            // DO something higher level
            return null;
        }
        return objetMathematique;
    }
}
