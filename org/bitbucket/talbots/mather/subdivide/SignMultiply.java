package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Multiplication;

/**
 * Created by Simon on 2016-03-07.
 */
public class SignMultiply implements ISign {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1, MathematicalObject obj2) {
        return new Multiplication(obj1, obj2);
    }
}
