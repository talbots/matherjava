package org.bitbucket.talbots.mather.subdivide;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-02.
 */
public class Signs extends SubdivisionClass {
    private SignInterface[] _lesSignes;
    public Signs(String str){
        super(str);
        _lesSignes = new SignInterface[4];
        _lesSignes[0] = new SignInterface(new SignAdd(), '+');
        _lesSignes[1] = new SignInterface(new SignSubstract(), '-');
        _lesSignes[2] = new SignInterface(new SignMultiply(), '*');
        _lesSignes[3] = new SignInterface(new SignDivide(), '/');

    }
    @Override
    public MathematicalObject GetMathematicalObjet() {
        return GetMathematicalObjet(_str, 0);
    }
    public MathematicalObject GetMathematicalObjet(String str, int valeurSigne) throws SyntaxException{
        if(valeurSigne >= _lesSignes.length){
            Words lesMots = new Words(str);
            return lesMots.GetMathematicalObjet();
        }
        int parenthesis = 0;
        String part = "";
        String[] parts = new String[2];
        boolean terminee = false;
        int i;
        for (i = 0; i < str.length() && !terminee; i++) {
            if(str.charAt(i) == '('){
                parenthesis++;
                part += "(";
            }
            else if(str.charAt(i) == ')'){
                parenthesis--;
                part += ")";
            }
            else if(str.charAt(i) == _lesSignes[valeurSigne].RetournerChar() && parenthesis == 0){
                terminee = true;
            }
            else{
                part += str.charAt(i);
            }
        }
        if(terminee){
            if(part != ""){
                parts[0] = part;
                part = "";
                for(int x = i; x < str.length(); x++){
                    part += str.charAt(x);
                }
                if(part != ""){
                    parts[1] = part;
                }
                else{
                    throw new SyntaxException("Aucun contenu après le signe");
                }

            }
            else{
                throw new SyntaxException("Aucun contenu avant le signe");
            }
        }
        else{
            return GetMathematicalObjet(str, valeurSigne+1);
        }
        //Une interface
        return _lesSignes[valeurSigne].RetournerObjetMathematique(
                GetMathematicalObjet(parts[0], valeurSigne+1),
                GetMathematicalObjet(parts[1], valeurSigne)
        );

    }
}
