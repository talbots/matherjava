package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public interface IFonction {
    MathematicalObject RetournerObjetMathematique(MathematicalObject obj1);
}
