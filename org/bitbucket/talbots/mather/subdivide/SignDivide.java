package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.Division;
import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public class SignDivide implements ISign {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1, MathematicalObject obj2) {
        return new Division(obj1, obj2);
    }
}
