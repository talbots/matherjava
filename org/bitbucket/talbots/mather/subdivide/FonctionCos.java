package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.Cos;
import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public class FonctionCos implements IFonction {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1) {
        return new Cos(obj1);

    }
}
