package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public interface ISign {
    MathematicalObject RetournerObjetMathematique(MathematicalObject obj1, MathematicalObject obj2);
}
