package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;
import org.bitbucket.talbots.mather.Sin;

/**
 * Created by Simon on 2016-03-07.
 */
public class FonctionSin implements IFonction {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1) {
        return new Sin(obj1);

    }
}
