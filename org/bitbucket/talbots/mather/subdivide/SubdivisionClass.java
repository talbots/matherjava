package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.*;

/**
 * Created by Simon on 2016-03-02.
 */
public abstract class SubdivisionClass {
    public String _str;
    public SubdivisionClass(String str){
        _str = str;
    }
    public abstract MathematicalObject GetMathematicalObjet();
}
