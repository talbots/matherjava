package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public class FonctionInterface {
    private String _mot;
    private IFonction _uneInterface;
    public FonctionInterface(IFonction uneInterface, String mot){
        _mot = mot;
    }
    public String RetournerMot(){
        return _mot;
    }
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1){
        return _uneInterface.RetournerObjetMathematique(obj1);
    }
}
