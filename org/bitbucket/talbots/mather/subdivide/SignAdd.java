package org.bitbucket.talbots.mather.subdivide;

import org.bitbucket.talbots.mather.Add;
import org.bitbucket.talbots.mather.MathematicalObject;

/**
 * Created by Simon on 2016-03-07.
 */
public class SignAdd implements ISign {
    @Override
    public MathematicalObject RetournerObjetMathematique(MathematicalObject obj1, MathematicalObject obj2) {
        return new Add(obj1, obj2);
    }
}
