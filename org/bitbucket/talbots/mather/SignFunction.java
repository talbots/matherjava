package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public abstract class SignFunction extends Function {
    public SignFunction(MathematicalObject obj1, MathematicalObject obj2){
        MathematicalObject[] lesObjects = new MathematicalObject[2];
        lesObjects[0] = obj1;
        lesObjects[1] = obj2;
        _innerParts = lesObjects;
    }
}
