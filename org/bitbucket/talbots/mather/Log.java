package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Log extends SimpleFunction {
    public Log(MathematicalObject obj1){
        super(obj1);
    }
    @Override
    public MathematicalObject Derive() {
        return new Division(
                _innerParts[0].Derive(),
                new Multiplication(
                        _innerParts[0],
                        new Ln(
                                new Const(10)
                        )
                )
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.log10(lesResultats[0]);
    }
}
