package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Ln extends SimpleFunction {
    public Ln(MathematicalObject obj1){
        super(obj1);
    }
    @Override
    public MathematicalObject Derive() {
        return new Division(
                _innerParts[0].Derive(),
                _innerParts[0]
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.log(lesResultats[0]);
    }
}
