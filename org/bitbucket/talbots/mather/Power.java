package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Power extends SignFunction {
    public Power(MathematicalObject obj1, MathematicalObject obj2){
        super(obj1, obj2);
    }
    @Override
    public MathematicalObject Derive() {
        MathematicalObject middlePart;
        if(!_innerParts[0].IsConstant()
                && !_innerParts[1].IsConstant())
        {
            middlePart = new Add(
                    new Multiplication(
                            new Multiplication(
                                    _innerParts[0],
                                    new Ln(_innerParts[0])
                            ),
                            _innerParts[1].Derive()
                    ),
                    new Multiplication(
                            _innerParts[1],
                            _innerParts[0].Derive()
                    )
            );
        }
        else if(!_innerParts[0].IsConstant()){
            middlePart = new Multiplication(
                    new Multiplication(
                            _innerParts[0],
                            new Ln(_innerParts[0])
                    ),
                    _innerParts[1].Derive()
            );
        }
        else{
            middlePart = new Multiplication(
                    _innerParts[1],
                    _innerParts[0].Derive()
            );
        }
        return new Multiplication(
                new Power(
                        _innerParts[0],
                        new Substract(
                                _innerParts[1],
                                new Const(1)
                        )
                ),
                middlePart
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.pow(lesResultats[0], lesResultats[1]);
    }
}
