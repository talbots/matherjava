package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Sqrt extends SimpleFunction {
    public Sqrt(MathematicalObject obj1){
        super(obj1);
    }
    @Override
    public MathematicalObject Derive() {
        return new Division(
                _innerParts[0].Derive(),
                new Multiplication(
                        new Const(2),
                        new Sqrt(_innerParts[0])
                )
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.sqrt(lesResultats[0]);
    }
}
