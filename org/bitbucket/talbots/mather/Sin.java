package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Sin extends SimpleFunction {
    public Sin(MathematicalObject obj1){
        super(obj1);
    }
    @Override
    public MathematicalObject Derive() {
        return new Multiplication(
                new Cos(_innerParts[0]),
                _innerParts[0].Derive()
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.sin(lesResultats[0]);
    }
}
