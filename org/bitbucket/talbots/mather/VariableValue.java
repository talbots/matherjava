package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class VariableValue {
    private String _variable;
    public double _value;
    public VariableValue(String variable, double value){
        _variable = variable;
        _value = value;
    }
    public String GetVariable(){
        return _variable;
    }
    public double GetValue(){
        return _value;
    }
}
