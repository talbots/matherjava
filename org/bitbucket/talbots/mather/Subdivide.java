package org.bitbucket.talbots.mather;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;

/**
 * Created by Simon on 2016-02-28.
 */
public class Subdivide {
    public static String[] Words(String str, String word) throws SyntaxException{
        int parenthesis = 0;
        String part = "";
        String[] parts = new String[2];
        boolean terminee = false;
        boolean valid;
        int i;
        for(i = 0; i< str.length() && !terminee; i++){
            if(str.charAt(i) == '('){
                parenthesis++;
                part += "(";
            }
            else if(str.charAt(i) == ')'){
                parenthesis--;
                part += ")";
            }
            else{
                valid = true;
                for(int j = 0; j < word.length() && valid; j++){
                    if(word.charAt(j) != str.charAt(i+j)){
                        valid = false;
                    }
                }
                if(valid){
                    terminee = true;
                }
                else{
                    part += str.charAt(i);
                }
            }
        }
        if(terminee){
            if(part != ""){

            }
            else{
                throw new SyntaxException("Aucun contenu avant le signe");
            }
        }
        else{
            throw new SyntaxException("Aucun mot");
        }

    }
}
