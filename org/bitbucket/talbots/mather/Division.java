package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Division extends SignFunction {
    public Division(MathematicalObject obj1, MathematicalObject obj2){
        super(obj1, obj2);
    }
    @Override
    public MathematicalObject Derive() {
        return new Division(
                new Substract(
                       new Multiplication(
                               _innerParts[0].Derive(),
                               _innerParts[1]
                       ),
                       new Multiplication(
                               _innerParts[0],
                               _innerParts[1].Derive()
                       )
                ),
                new Power(
                        _innerParts[1],
                        new Const(2)
                )
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return lesResultats[0] / lesResultats[1];
    }
}
