package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Var extends MathematicalObject {
    private String _symbol;
    public Var(String symbol){
        _symbol = symbol;
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        for(int i = 0; i < lesObjets.length; i++){
            if(lesObjets[i].GetObject().getClass() == Var.class
                    && _symbol == ((Var)lesObjets[i].GetObject())._symbol ){
                return lesObjets[i].GetValue();
            }
        }
        return 0;
    }
    @Override
    public MathematicalObject Derive() {
        return new Const(1);
    }
    @Override
    public boolean Replace(MathematicalObject partToReplace, MathematicalObject part) {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == Var.class && ((Var)obj)._symbol == _symbol){
            return true;
        }
        return false;
    }

    @Override
    public boolean IsConstant() {
        return false;
    }
}
