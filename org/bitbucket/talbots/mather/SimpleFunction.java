package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public abstract class SimpleFunction extends Function {
    public SimpleFunction(MathematicalObject obj1){
        MathematicalObject[] lesObjects = new MathematicalObject[1];
        lesObjects[0] = obj1;
        _innerParts = lesObjects;
    }
}
