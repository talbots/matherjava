package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Cos extends SimpleFunction {
    public Cos(MathematicalObject obj1){
        super(obj1);
    }
    @Override
    public MathematicalObject Derive() {
        return new Substract(
                new Const(0),
                new Multiplication(
                        new Sin(_innerParts[0]),
                        _innerParts[0].Derive()
                )
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.cos(lesResultats[0]);
    }
}
