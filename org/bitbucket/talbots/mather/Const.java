package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Const extends MathematicalObject {
    public double _value;
    public String _identifier;
    public Boolean _isIdentifier;
    public Const(double value){
        _value = value;
        _isIdentifier = false;
        _identifier = "nan";
    }
    public Const(double value, String identifier){
        _value = value;
        _identifier = identifier;
        _isIdentifier = true;

    }
    @Override
    public boolean Replace(MathematicalObject partToReplace, MathematicalObject part) {
        return false;
    }
    @Override
    public MathematicalObject Derive() {
        return new Const(0);
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        return _value;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() == Const.class
                && (_value == ((Const) obj)._value
                || _identifier == ((Const) obj)._identifier))
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean IsConstant() {
        return true;
    }
}
