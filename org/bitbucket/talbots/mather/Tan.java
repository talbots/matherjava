package org.bitbucket.talbots.mather;

/**
 * Created by Simon on 2016-02-28.
 */
public class Tan extends SimpleFunction{
    public Tan(MathematicalObject obj1){
        super(obj1);
    }
    @Override
    public MathematicalObject Derive() {
        return new Division(
                _innerParts[0].Derive(),
                new Power(
                        new Cos(_innerParts[0]),
                        new Const(2)
                )
        );
    }
    @Override
    public double GetResult(ObjectValue[] lesObjets) {
        double[] lesResultats = GetPartsResult(lesObjets);
        return Math.tan(lesResultats[0]);
    }
}
